{
    "id": "463b0089-e4b2-4c8e-8323-3ee877c8920c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76981051-93cc-465d-877a-da17e135e9d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "463b0089-e4b2-4c8e-8323-3ee877c8920c",
            "compositeImage": {
                "id": "cb2b9eb9-3202-467b-a34f-eb19c3aa513f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76981051-93cc-465d-877a-da17e135e9d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4580c3c-95fb-4126-a8ed-a2fdf8cadcd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76981051-93cc-465d-877a-da17e135e9d1",
                    "LayerId": "44dc50cc-f40e-4c4d-90f9-a74f9e508adb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44dc50cc-f40e-4c4d-90f9-a74f9e508adb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "463b0089-e4b2-4c8e-8323-3ee877c8920c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}