{
    "id": "34be7a3a-fc9e-42d4-bff0-dac86e9cb0e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb6c06c7-490d-41c2-b654-a27d6ce9ffa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34be7a3a-fc9e-42d4-bff0-dac86e9cb0e0",
            "compositeImage": {
                "id": "0c70db03-5bb6-4cd7-8a41-8c75b1f203d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6c06c7-490d-41c2-b654-a27d6ce9ffa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5c98e2-f5f5-4048-a8d8-160071f2e8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6c06c7-490d-41c2-b654-a27d6ce9ffa7",
                    "LayerId": "15159dc8-be2b-441c-a91c-e720f2ce6c7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "15159dc8-be2b-441c-a91c-e720f2ce6c7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34be7a3a-fc9e-42d4-bff0-dac86e9cb0e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}