{
    "id": "e6878e71-ddd2-42ab-bac5-99a74047a681",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "RUNmainchar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 8,
    "bbox_right": 54,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "352073ed-22be-4428-9fe3-03f82e03a8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "7d5f337a-7312-4db2-a8cf-0f614a923a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "352073ed-22be-4428-9fe3-03f82e03a8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c64cda5-285a-419f-8443-0dae55161770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "352073ed-22be-4428-9fe3-03f82e03a8b1",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "5c406639-f0bb-4be1-acde-7b28d77f025b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "dfeed530-caeb-448c-9f0c-89b9f15f14dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c406639-f0bb-4be1-acde-7b28d77f025b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b118d81e-4b8a-4d8d-b8e6-11cd5fa6d1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c406639-f0bb-4be1-acde-7b28d77f025b",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "fbb4432e-f79f-40e9-a8ee-ce320e4a779f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "f27e8266-6e59-4ca7-a3a5-e443f730f3d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb4432e-f79f-40e9-a8ee-ce320e4a779f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dfe2a7a-d37e-47b5-99cb-593c1eec1479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb4432e-f79f-40e9-a8ee-ce320e4a779f",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "9dbe00fb-8d4b-4fa6-8643-9ac18ea3395e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "b4b39e14-28a5-4914-a262-62619338ccf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dbe00fb-8d4b-4fa6-8643-9ac18ea3395e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18429472-0d5f-44d7-9fc3-db7e5fa33aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dbe00fb-8d4b-4fa6-8643-9ac18ea3395e",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "1971c8c4-413d-42af-9a85-fda57a2ce5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "e1f697cd-6964-4909-a44e-96cbd652935b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1971c8c4-413d-42af-9a85-fda57a2ce5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ebcce62-ebb2-4f51-9ca1-9d76e73e9d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1971c8c4-413d-42af-9a85-fda57a2ce5e8",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "b6bd491a-ce23-4c67-bed4-29b39a328dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "5d8271d3-f427-4250-8aab-f7f952fc3ab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6bd491a-ce23-4c67-bed4-29b39a328dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a0eab3-5199-4e96-86a5-747144e2347b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6bd491a-ce23-4c67-bed4-29b39a328dfe",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "0c6d4843-28f3-4c5f-af13-31c708811f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "2c4d248b-0f6a-42b2-8595-6bc4c9db4313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c6d4843-28f3-4c5f-af13-31c708811f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a5b7bd-dea8-4e6a-8680-2f287afd0640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c6d4843-28f3-4c5f-af13-31c708811f9f",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        },
        {
            "id": "ce8a6a1f-29e8-48a1-87de-c418c090d27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "compositeImage": {
                "id": "ca8b6dcb-1228-4db5-8776-4568e57dc4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8a6a1f-29e8-48a1-87de-c418c090d27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04bffd98-b6e5-4f3a-86e5-b9f49a62ac81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8a6a1f-29e8-48a1-87de-c418c090d27e",
                    "LayerId": "dae0ef5f-fd8a-40f8-a627-745618c9a932"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dae0ef5f-fd8a-40f8-a627-745618c9a932",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6878e71-ddd2-42ab-bac5-99a74047a681",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}