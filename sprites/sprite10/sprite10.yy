{
    "id": "9e161a92-ec77-4031-b3a9-b38f027dd126",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8b8ba2c-885a-428e-affc-34b0809b6943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e161a92-ec77-4031-b3a9-b38f027dd126",
            "compositeImage": {
                "id": "189977ca-834d-429d-858e-56aee2c51a44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b8ba2c-885a-428e-affc-34b0809b6943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e1feb0e-ed04-4fcc-bb8e-e68c7cfe3bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b8ba2c-885a-428e-affc-34b0809b6943",
                    "LayerId": "06cbecb0-114e-402a-a4fc-96f295e774d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "06cbecb0-114e-402a-a4fc-96f295e774d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e161a92-ec77-4031-b3a9-b38f027dd126",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}