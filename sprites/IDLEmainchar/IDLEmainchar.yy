{
    "id": "177335a1-570b-4dad-b1c6-e395edb3a197",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "IDLEmainchar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 31,
    "bbox_right": 31,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98b5b118-297f-4392-8680-a30829bd1215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
            "compositeImage": {
                "id": "382d7931-7150-4554-98b3-1b7863bb9249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b5b118-297f-4392-8680-a30829bd1215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b7176a-d7ca-4b29-a121-7ac6dc20fa1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b5b118-297f-4392-8680-a30829bd1215",
                    "LayerId": "174c1cd0-d1b0-4195-ac8f-9a4416770eec"
                }
            ]
        },
        {
            "id": "f0cbd4b3-d52c-4f34-87a6-c0088b89110b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
            "compositeImage": {
                "id": "dabe5f5e-8525-4de4-992c-0d31f44738bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cbd4b3-d52c-4f34-87a6-c0088b89110b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a465b789-38a2-4b43-b1c5-9e1824f0b789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cbd4b3-d52c-4f34-87a6-c0088b89110b",
                    "LayerId": "174c1cd0-d1b0-4195-ac8f-9a4416770eec"
                }
            ]
        },
        {
            "id": "bf2a3e6d-1150-403e-9135-dcb21740b4d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
            "compositeImage": {
                "id": "876e426e-790a-4374-8576-d255e32d65c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf2a3e6d-1150-403e-9135-dcb21740b4d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc864163-9031-4950-867b-223e459cedc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf2a3e6d-1150-403e-9135-dcb21740b4d6",
                    "LayerId": "174c1cd0-d1b0-4195-ac8f-9a4416770eec"
                }
            ]
        },
        {
            "id": "0b49ab3a-347d-4234-8200-45dbf71d3f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
            "compositeImage": {
                "id": "c27ca11e-2969-4a36-8ae8-2ff62482b859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b49ab3a-347d-4234-8200-45dbf71d3f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab892ce4-b4c8-46ff-aeab-874026f2d7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b49ab3a-347d-4234-8200-45dbf71d3f04",
                    "LayerId": "174c1cd0-d1b0-4195-ac8f-9a4416770eec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "174c1cd0-d1b0-4195-ac8f-9a4416770eec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}