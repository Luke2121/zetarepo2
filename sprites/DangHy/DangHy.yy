{
    "id": "16d4af9c-8ab8-46db-806f-a96e240df951",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "DangHy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e7161e9-5025-4342-b93f-bcbe62f099c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16d4af9c-8ab8-46db-806f-a96e240df951",
            "compositeImage": {
                "id": "4d199b7a-5f97-49e2-8a9b-274631a3d830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e7161e9-5025-4342-b93f-bcbe62f099c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9df21f-697c-4acd-a83a-0ce9baf242a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e7161e9-5025-4342-b93f-bcbe62f099c9",
                    "LayerId": "5cc91f00-ff76-4060-95e5-e3b981d3ce58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5cc91f00-ff76-4060-95e5-e3b981d3ce58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16d4af9c-8ab8-46db-806f-a96e240df951",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}