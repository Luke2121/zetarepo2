{
    "id": "1ff9d6b0-62b3-4dbb-b028-1620b42519a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26296180-e09f-4bea-97b4-a9675e32deaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ff9d6b0-62b3-4dbb-b028-1620b42519a9",
            "compositeImage": {
                "id": "52cf709d-431c-417c-8165-5647751fcba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26296180-e09f-4bea-97b4-a9675e32deaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a87e6f3-7d50-473d-baaa-340343fb09b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26296180-e09f-4bea-97b4-a9675e32deaf",
                    "LayerId": "bf3da096-df0e-4c58-a159-33b07d159241"
                }
            ]
        },
        {
            "id": "e29ce47a-ddeb-4a6c-9a7f-5d7d21e04d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ff9d6b0-62b3-4dbb-b028-1620b42519a9",
            "compositeImage": {
                "id": "93068db3-9003-4328-bbdd-a84880e43911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e29ce47a-ddeb-4a6c-9a7f-5d7d21e04d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77010d38-c3a6-4ff5-af43-a8c90ff1ede6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e29ce47a-ddeb-4a6c-9a7f-5d7d21e04d6a",
                    "LayerId": "bf3da096-df0e-4c58-a159-33b07d159241"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "bf3da096-df0e-4c58-a159-33b07d159241",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ff9d6b0-62b3-4dbb-b028-1620b42519a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}