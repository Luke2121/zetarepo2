{
    "id": "6f06f90b-1356-46b2-b70b-e467d8bb81f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dba5ea58-a65b-4de3-9c43-4f588e38d68b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f06f90b-1356-46b2-b70b-e467d8bb81f6",
            "compositeImage": {
                "id": "d00aae27-0995-4d16-932c-5c370f3ed1a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dba5ea58-a65b-4de3-9c43-4f588e38d68b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ba07ff-e26f-4a08-9ed3-f6aeca06bf84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dba5ea58-a65b-4de3-9c43-4f588e38d68b",
                    "LayerId": "e0e34db7-f0b9-4a04-9c86-c29550af81ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e0e34db7-f0b9-4a04-9c86-c29550af81ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f06f90b-1356-46b2-b70b-e467d8bb81f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}