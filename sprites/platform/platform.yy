{
    "id": "8f673c4e-2d74-4cc3-900b-81d3e619f6d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d130dd7e-55ee-4859-9d85-039f40ab963c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f673c4e-2d74-4cc3-900b-81d3e619f6d4",
            "compositeImage": {
                "id": "65f9ff06-6a21-4076-b5f4-0cd4e7d557e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d130dd7e-55ee-4859-9d85-039f40ab963c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3f6425-7344-4d44-976b-ad2b68742c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d130dd7e-55ee-4859-9d85-039f40ab963c",
                    "LayerId": "ee46283a-a129-4d9c-9b22-fa649c118eef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ee46283a-a129-4d9c-9b22-fa649c118eef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f673c4e-2d74-4cc3-900b-81d3e619f6d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}