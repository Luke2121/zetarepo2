{
    "id": "8fcb69c1-e339-49e9-a62e-2d52297e502a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "RyanRod",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 15,
    "bbox_right": 54,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5575a542-1a69-453c-9f1c-2c3283138dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fcb69c1-e339-49e9-a62e-2d52297e502a",
            "compositeImage": {
                "id": "2f7cde1f-76bb-4c8b-ad7c-624fc2f0998b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5575a542-1a69-453c-9f1c-2c3283138dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "696e70bd-be39-4b71-9a2d-0afdcb45b87f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5575a542-1a69-453c-9f1c-2c3283138dfa",
                    "LayerId": "e21c4b1f-9b97-4bcb-9724-a008b30d1284"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e21c4b1f-9b97-4bcb-9724-a008b30d1284",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fcb69c1-e339-49e9-a62e-2d52297e502a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}