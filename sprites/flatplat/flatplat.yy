{
    "id": "1fe90a90-cd83-46b6-b8ab-a0795bac7e9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flatplat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a94900d-0e27-451f-a8a9-0bead21cf399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fe90a90-cd83-46b6-b8ab-a0795bac7e9b",
            "compositeImage": {
                "id": "a024d4cd-924a-4080-9503-e650bcade544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a94900d-0e27-451f-a8a9-0bead21cf399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe9505a8-4603-4d26-a622-af793ad3be41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a94900d-0e27-451f-a8a9-0bead21cf399",
                    "LayerId": "375a0c21-7300-49f3-919a-3b0a5d2c3ac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "375a0c21-7300-49f3-919a-3b0a5d2c3ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fe90a90-cd83-46b6-b8ab-a0795bac7e9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}