{
    "id": "d1eb5153-d449-4b37-8bc0-121fdf4c4435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 7,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5407e3df-b0ae-4f0d-a0b5-848d9b03c227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1eb5153-d449-4b37-8bc0-121fdf4c4435",
            "compositeImage": {
                "id": "189d54c5-317f-4cb8-a0dc-a0661a6cb8fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5407e3df-b0ae-4f0d-a0b5-848d9b03c227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14dc192f-917d-446f-bda0-9c88da17b8cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5407e3df-b0ae-4f0d-a0b5-848d9b03c227",
                    "LayerId": "09c4f536-1523-4dc6-a089-d28cf09aa042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "09c4f536-1523-4dc6-a089-d28cf09aa042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1eb5153-d449-4b37-8bc0-121fdf4c4435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 13,
    "yorig": 9
}