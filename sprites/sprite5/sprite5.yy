{
    "id": "b769c526-8b63-41f1-a3a9-d4a3bf028a30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e6c0611-79d2-4c50-931a-836f2b7ddf19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b769c526-8b63-41f1-a3a9-d4a3bf028a30",
            "compositeImage": {
                "id": "b281e2dc-cd67-4509-ad1c-d1eec264ebe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6c0611-79d2-4c50-931a-836f2b7ddf19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3743cd65-552b-4f4d-96c1-b9f997d043de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6c0611-79d2-4c50-931a-836f2b7ddf19",
                    "LayerId": "3bf6b09d-492b-43a5-86d6-fde217ad98a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3bf6b09d-492b-43a5-86d6-fde217ad98a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b769c526-8b63-41f1-a3a9-d4a3bf028a30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}