/// @description Insert description here
// You can write your code in this editor

//set controls
keyright = keyboard_check(vk_right) || keyboard_check(ord("D"));
keyleft = keyboard_check(vk_left) || keyboard_check(ord("A"));
jump = keyboard_check_pressed(vk_space);
keydown = keyboard_check(vk_down);
keyleftup = keyboard_check_released(vk_left);
keyrightup = keyboard_check_released(vk_right); 



//state variables

//slide
if(keyrightup){ slide = walksp; sliding = true;}

if(slide > 0) slide -= .2;

if(keyleftup) {slide = -walksp; sliding = true;}

if(slide < 0) slide += .2;

if(keyleft || keyright){ sliding = false; slide = 0; }

//horizontal movement
if (!walljumping) hsp = ((keyright - keyleft) * walksp) + slide;





//vertical movement

if(jump && grounded) 
{ vsp = -11 injump = true}

 vsp += grv;

//walljumping

if(place_meeting(x+10*image_xscale,y,object1) && jump && grounded = false){ vsp = -8 hsp = -image_xscale*4 walljumping = true; image_xscale *= -1 }

if(grounded) walljumping = false;



//Horizontal Collision

//walkup slopes


if (place_meeting(x+hsp,y,object1))
{
    yplus = 0;
    while (place_meeting(x+hsp,y-yplus,object1) && yplus <= abs(1*hsp)) yplus += 1;
    if place_meeting(x+hsp,y-yplus,object1)
    {
        while (!place_meeting(x+sign(hsp),y,object1)) x+=sign(hsp);
        hsp = 0;
    }
    else
    {
        y -= yplus
    }
}

//sloped oneway platforms horizontal collision with slopes
if (place_meeting(x+hsp,bbox_bottom,slopedplat))
{
    yplus = 0;
    while (place_meeting(x+hsp,y-yplus,slopedplat) && yplus <= abs(1*hsp)) yplus += 1;
    if place_meeting(x+hsp,y-yplus,slopedplat)
    {
        while (!place_meeting(x+sign(hsp),y,slopedplat)) x+=sign(hsp);
        hsp = 0;
    }
    else
    {
        y -= yplus
    }
}	



//walkdown slopes
if (!place_meeting(x+hsp,y+1,object1) && grounded)
{
    yminus = 0;
    while (!place_meeting(x+hsp,y+yminus,object1)) yminus += 1;
     if(yminus < 5)  y += yminus
   
}







x += hsp;
	


//vertical collision

if (place_meeting(x,y+vsp,object1))
{
	while(!place_meeting(x,y+sign(vsp),object1)) y += sign(vsp);
	vsp = 0;
	grounded = true;
	
}
else if (place_meeting(x,y+vsp,oplatform)) grounded = true;
else grounded = false;


//sloped oneway platforms
if (place_meeting(x,bbox_bottom+vsp,slopedplat) && vsp >= 0)
{
	while(!place_meeting(x,y+sign(vsp),slopedplat)) y += sign(vsp);
	vsp = 0;
	
	
}
//falls through platform
if(place_meeting(x,bbox_bottom+vsp,slopedplat) && keydown)
{
	if(place_meeting(x,bbox_bottom,slopedplat)) vsp = 6;
}

if (place_meeting(x,bbox_bottom,slopedplat)) grounded = true;

	
y += vsp;




	

	
	
//animations

//turns around
if(keyright && !walljumping) image_xscale = 1;
if(keyleft && !walljumping) image_xscale = -1;


//general state anims
if(hsp != 0 && grounded && !sliding) sprite_index = RUNmainchar;
else sprite_index = IDLEmainchar;