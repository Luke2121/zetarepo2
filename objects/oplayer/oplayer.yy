{
    "id": "8cf7cb94-e7f8-45d4-a7b1-3e080ec92152",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oplayer",
    "eventList": [
        {
            "id": "6bee2c97-f2b3-4320-be06-424b91198400",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8cf7cb94-e7f8-45d4-a7b1-3e080ec92152"
        },
        {
            "id": "17863f2a-3519-4105-a91b-b2ac5bf8adba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8cf7cb94-e7f8-45d4-a7b1-3e080ec92152"
        },
        {
            "id": "5c0d0015-dc22-479a-85f7-58239a60af51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8cf7cb94-e7f8-45d4-a7b1-3e080ec92152"
        }
    ],
    "maskSpriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "177335a1-570b-4dad-b1c6-e395edb3a197",
    "visible": true
}