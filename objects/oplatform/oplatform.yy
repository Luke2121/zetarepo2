{
    "id": "2cb2fe6b-2e99-4165-9552-fe6c0fc5bf0a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oplatform",
    "eventList": [
        {
            "id": "57c34b0f-b7f9-43f5-aa14-5e9b881c72fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2cb2fe6b-2e99-4165-9552-fe6c0fc5bf0a"
        },
        {
            "id": "e50d1dbe-879a-4b94-8551-b18110132d8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2cb2fe6b-2e99-4165-9552-fe6c0fc5bf0a"
        },
        {
            "id": "eb7c3aed-e328-474c-b5d9-66277517ee6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2cb2fe6b-2e99-4165-9552-fe6c0fc5bf0a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b8efd8a0-3250-4f77-aa3e-62151359f1b1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1fe90a90-cd83-46b6-b8ab-a0795bac7e9b",
    "visible": true
}