{
    "id": "8b309d0b-56e2-4974-b206-e0e6e9951468",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "dfa66792-6d07-487b-994f-833df34b4edc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8b309d0b-56e2-4974-b206-e0e6e9951468"
        },
        {
            "id": "9a6f7b00-cec9-4e64-a81e-c2d6f16ad9c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "8b309d0b-56e2-4974-b206-e0e6e9951468"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1ff9d6b0-62b3-4dbb-b028-1620b42519a9",
    "visible": true
}