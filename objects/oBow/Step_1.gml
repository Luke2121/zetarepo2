/// @description Insert description here
// You can write your code in this editor

x = oplayer.x;
y = oplayer.y;

image_angle = point_direction(x,y,mouse_x,mouse_y);
firingdelay = firingdelay - 1;
recoil = max(0,recoil - 1);

if (mouse_check_button(mb_left)) && (firingdelay < 0)
{
	recoil = 4;
	firingdelay = 5;
	with (instance_create_layer(x,y,"Bullets",oBullet))
	{
		speed = 20;	
		direction = other.image_angle + random_range(-15,15);
		image_angle = direction;
	}
}

//The Bow recoil
x = x - lengthdir_x(recoil,image_angle);
y = y - lengthdir_y(recoil,image_angle);


//Flips bow angle
if (image_angle > 90) && (image_angle < 270)
{
	image_yscale = -1;
}
else
{
	image_yscale = 1;
}