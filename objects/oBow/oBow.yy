{
    "id": "c9c70821-4446-46bd-aa35-ea1681ad712b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBow",
    "eventList": [
        {
            "id": "ae20ddc1-bd25-466d-b527-3725c16ea869",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c9c70821-4446-46bd-aa35-ea1681ad712b"
        },
        {
            "id": "326e5f8b-6d70-427f-808b-c8a1fd3c42ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9c70821-4446-46bd-aa35-ea1681ad712b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1eb5153-d449-4b37-8bc0-121fdf4c4435",
    "visible": true
}